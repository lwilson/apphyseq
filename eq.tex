\documentclass[12pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{siunitx}
\usepackage{unitmacros}

\usepackage{fontspec}
\setromanfont{Cormorant}
\setmonofont{Fira Mono}
\newfontfamily{\slabserif}{Bitter Thin}
\newfontfamily{\slabserifheavy}{Bitter}

\usepackage{titlesec}
\titleformat{\section}{\Large\slabserifheavy}{}{0em}{}

\def\arraystretch{1.5}

\usepackage{anyfontsize}
\usepackage{titling}
\title{AP Physics 1}
\author{Leo Wilson}
\date{10 May 2021}
\renewcommand{\maketitle}{
  \begin{center}
    {\fontsize{80}{96}\selectfont\slabserif \thetitle}

    \vspace{1em}

    {\fontsize{35}{42}\selectfont\slabserif Equation Sheet}
  \end{center}
}

\begin{document}

  \null\vspace{16em}

  \maketitle

  \vspace*{\fill}

  \begin{center}
    Typeset by {\theauthor} in Cormorant and Bitter using {\LaTeX}. CC-BY-SA.

    \vspace{0.5em}

    \tiny \texttt{https://lwilson.dev/}
  \end{center}

  \newpage

    \section{Kinematics}
    \begin{align*}
      v &= v_0 + at & v^2 = v^2_0 + 2a \Delta x \\
      \Delta x &= v_0 t + \frac{1}{2} a t^2 & \Delta x = \left(\frac{v + v_0}{2}\right) t
    \end{align*}
    where
    \begin{align*}
      v &= \mbox{velocity} \umps & v_0 &= \mbox{initial velocity} \umps \\
      a &= \mbox{acceleration} \umpss & t &= \mbox{time} \us
    \end{align*}

  \section{Projectile Motion}
  \begin{align*}
    y &= y_0 + v_{y0} t - \frac{1}{2} g t^2 & x &= x_0 + v_{x0} t
  \end{align*}
  where
  \begin{align*}
    y &= \mbox{vertical position} \um & y_0 &= \mbox{initial vertical position} \um \\
    v_{y0} &= \mbox{initial vertical velocity} \umps & t &= \mbox{time} \us \\
    x &= \mbox{horizontal position} \um & x_0 &= \mbox{initial vertical position} \um \\
    v_{x0} &= \mbox{initial horizontal velocity} \umps
    & g &= \mbox{gravitational acceleration} = 9.8 \umpss
  \end{align*}
  
  \section{Force}
  \begin{align*}
    F &= ma & a &= \frac{F}{m} & m &= \frac{F}{a}
  \end{align*}
  where
  \begin{align*}
    F &= \mbox{force} \uN & a &= \mbox{acceleration} \umpss & m &= \mbox{mass} \ukg
  \end{align*}
  
  \section{Friction}
  \[F_f \leq \mu F_N\]
  where
  $\mu = \mbox{the coefficient of kinetic or static friction}$

  \section{Springs}
  \begin{align*}
    F &= k \Delta x & U_s &= \frac{1}{2} F \Delta x = \frac{1}{2} k \Delta x^2
  \end{align*}
  where
  \begin{align*}
    F &= \mbox{force} \uN & k &= \mbox{spring constant} \uNpm \\
    \Delta x &= \mbox{displacement} \um & U_s &= \mbox{spring potential energy} \uJ
  \end{align*}

  \section{Kinetic Energy}
  \[K = \frac{1}{2} m v^2\]
  where
  \begin{align*}
    K &= \mbox{kinetic energy} \uJ & m &= \mbox{mass} \ukg & v &= \mbox{speed} \umps
  \end{align*}

  \section{Gravity}
  \begin{align*}
    F_g &= mg & g &= \frac{F_g}{m} \\
    F_g &= \frac{G m_1 m_2}{r^2} & g &= \frac{F_g}{m_1} = \frac{G m_2}{r^2} \\
    U_g &= -\frac{G m_1 m_2}{r} & \Delta U_g &= m g \Delta y \\
  \end{align*}
  where
  \begin{align*}
    F_g &= \mbox{gravitational force} \uN &
    \Delta U_g &= \mbox{change in gravitational potential energy} \uJ \\
    m &= \mbox{mass} \ukg & U_g &= \mbox{gravitational potential energy} \uJ \\
    \Delta y &= \mbox{vertical displacement} \um & r &= \mbox{distance} \um \\
    m_1 &= \mbox{mass of the first object} \ukg &
    m_2 &= \mbox{mass of the second object} \ukg
  \end{align*}
  \begin{align*}
    g &= \mbox{gravitational acceleration} = 9.8 \umpss \\
    G &= \mbox{universal gravitational constant} = 6.67 \cdot 10^{-11} \ummmpkgss
  \end{align*}

  \section{Work}
  \begin{align*}
    W &= \Delta E & W &= F d \left(\cos \theta\right) & W &= F_{\parallel} d
  \end{align*}
  where
  \begin{align*}
    W &= \mbox{work} \uJ & F &= \mbox{force} \uN \\
    d &= \mbox{distance} \um & F_{\parallel} &= \mbox{parallel force} \uN \\
    \theta &= \mbox{angle} \uangle
  \end{align*}

  \section{Power}
  \[P = \frac{W}{t}\]
  where
  \begin{align*}
    P &= \mbox{power} \uW & W &= \mbox{work} \uJ & t &= \mbox{time} \us
  \end{align*}

  \section{Momentum and Impulse}
  \begin{align*}
    p &= mv & J &= Ft & J &= \Delta p
  \end{align*}
  where
  \begin{align*}
    p &= \mbox{momentum} \ukgmps & J &= \mbox{impulse} \uNs \\
    m &= \mbox{mass} \ukg & v &= \mbox{velocity} \umps \\
    F &= \mbox{force} \uN & t &= \mbox{duration of impact} \us
  \end{align*}

  \section{Centre of Mass}
  \[x_{cm} = \frac{\sum mx}{\sum m}\]
  where
  \begin{align*}
    m &= \mbox{mass} \um & x &= \mbox{position} \um \\
    x_{cm} &= \mbox{position of the centre of mass} \um
  \end{align*}

  \section{Centripetal Force}
  \begin{align*}
    F_c &= \sum F & F_c &= m a_c = \frac{mv^2}{r} \\
    v &= \frac{2 \pi r}{T} & a_c &= \frac{v^2}{r}
  \end{align*}
  where
  \begin{align*}
    F_c &= \mbox{centripetal force} \uN & F &= \mbox{force} \uN \\
    m &= \mbox{mass} \ukg & v &= \mbox{velocity} \umps \\
    r &= \mbox{radius} \um & T &= \mbox{period} \us \\
    a_c &= \mbox{centripetal acceleration} \umpss
  \end{align*}

  \section{Angular Velocity}
  \begin{align*}
    \omega &= \frac{\Delta \theta}{t} &
    \Delta \theta &= \frac{\Delta s}{r} &
    \omega &= \frac{v}{r}
  \end{align*}
  where
  \begin{align*}
    \omega &= \mbox{angular velocity} \uangleps &
    \Delta \theta &= \mbox{angular displacement} \uangle \\
    t &= \mbox{time} \us & v &= \mbox{tangential velocity} \umps \\
    \Delta s &= \mbox{change in arc length} \um &
    r &= \mbox{radius} \um
  \end{align*}

  \section{Angular Kinematics}
  \begin{align*}
    \Delta \theta &= \frac{1}{2} \alpha t^2 + \omega_0 t &
    \omega &= \alpha t + \omega_0 &
    \omega^2 &= 2 \alpha \Delta \theta + \omega_0^2
  \end{align*}
  where
  \begin{align*}
    \omega &= \mbox{angular velocity} \uangleps &
    \Delta \theta &= \mbox{angular displacement} \uangle \\
    t &= \mbox{time} \us & \alpha &= \mbox{angular acceleration} \uanglepss \\
  \end{align*}

  \section{Torque}
  \begin{align*}
    \tau &= rF \sin \theta
  \end{align*}
  where
  \begin{align*}
    \tau &= \mbox{torque} \uNm & r &= \mbox{radius} \um \\
    F &= \mbox{force} \uN & \theta &= \mbox{angle} \uangle
  \end{align*}

  \section{Rotational Inertia}
  \begin{align*}
    \tau = I \alpha
  \end{align*}
  \begin{center}
    \begin{tabular}{ c c c }
      object & hollow & solid \\ \hline
      sphere & $I=\frac{2}{3}mr^2$ & $I=\frac{2}{5}mr^2$ \\
      disk or cylinder & $I=mr^2$ & $I=\frac{1}{2}mr^2$ \\
      very thin door & \multicolumn{2}{c}{$I=\frac{1}{3}mL^2$} \\
      very thin revolving door & \multicolumn{2}{c}{$I=\frac{1}{2}mL^2$} \\
      point & \multicolumn{2}{c}{$I=mr^2$}
    \end{tabular}
  \end{center}

  where
  \begin{align*}
    \tau &= \mbox{torque} \uNm &
    \alpha &= \mbox{angular acceleration} \uanglepss \\
    I &= \mbox{moment of inertia} \ukgmm &
    m &= \mbox{mass} \ukg \\
    r &= \mbox{radius} \um & L &= \mbox{length} \um
  \end{align*}

  \section{Rotational Kinetic Energy}
  \begin{align*}
    K_{rot} &= \frac{1}{2}I\omega^2
  \end{align*}
  where
  \begin{align*}
    K_{rot} &= \mbox{rotational kinetic energy} \uJ &
    I &= \mbox{moment of inertia} \ukgmm \\
    \omega &= \mbox{angular velocity} \uangleps
  \end{align*}

  \section{Angular Momentum}
  \begin{align*}
    L &= I \omega & \Delta L &= \tau t
  \end{align*}
  where
  \begin{align*}
    L &= \mbox{angular momentum} \ukgmmps &
    I &= \mbox{moment of inertia} \ukgmm \\
    \omega &= \mbox{angular velocity} \uangleps &
    \tau &= \mbox{torque} \uNm \\
    \Delta L &= \mbox{change in angular momentum} \ukgmmps &
    t &= \mbox{time} \us
  \end{align*}

  \section{Pendulum Oscillation}
  \begin{align*}
    T &= 2\pi \sqrt{\frac{l}{g}}
  \end{align*}
  where
  \begin{align*}
    T &= \mbox{period} \us & l &= \mbox{length} \um &
    g &= \mbox{gravitational acceleration} = 9.8 \umpss
  \end{align*}

  \section{Spring Oscillation}
  \begin{align*}
    T &= 2\pi \sqrt{\frac{m}{k}}
  \end{align*}
  where
  \begin{align*}
    T &= \mbox{period} \us & m &= \mbox{mass} \ukg & k &= \mbox{spring constant} \uNpm
  \end{align*}

  \section{Common Values of Trigonometric Functions}
  \begin{center}
    $\begin{array}{ c | c c c c c c c }
      \theta & \ang{0} & \ang{30} & \ang{37} & \ang{45} & \ang{53} & \ang{60} & \ang{90} \\
      \hline
      \sin \theta & 0 & 1/2 & 3/5 & \sqrt{2}/2 & 4/5 & \sqrt{3}/2 & 1 \\
      \cos \theta & 1 & \sqrt{3}/2 & 4/5 & \sqrt{2}/2 & 3/5 & 1/2 & 0 \\
      \tan \theta & 0 & \sqrt{3}/3 & 3/4 & 1 & 4/3 & \sqrt{3} & \infty
    \end{array}$
  \end{center}

  \section{Waves}
  \begin{align*}
    v &= \lambda f &
    v &= \frac{\lambda}{T} \\
    T &= \frac{1}{f} &
    v &= \sqrt{\frac{F_T}{\mu}}
  \end{align*}
  where
  \begin{align*}
    v &= \mbox{wave speed} \umps &
    \lambda &= \mbox{wavelength} \um \\
    T &= \mbox{period} \us &
    f &= \mbox{frequency} \uHz \\
    F_T &= \mbox{tension force} \uN &
    \mu &= \mbox{linear mass density} = \frac{m}{L} \ukgpm \\
    m &= \mbox{mass} \ukg & L &= \mbox{length} \um
  \end{align*}

  \section{Doppler Effect}
  \begin{align*}
    f_{obs} &= \frac{c}{c + v} \cdot f_{src}
  \end{align*}
  where
  \begin{align*}
    f_{obs} &= \mbox{observed frequency} \uHz &
    f_{src} &= \mbox{emitted frequency of source} \uHz \\
    c &= \mbox{speed of sound} \umps &
    v &= \mbox{velocity of source relative to observer} \umps
  \end{align*}

  \section{Charge}
  \begin{align*}
    F_e &= \frac{kq_1q_2}{r^2}
  \end{align*}
  where
  \begin{align*}
    F_e &= \mbox{static electric force} \uN &
    r &= \mbox{distance} \um \\
    q_{\{1,2\}} &= \mbox{charges} \uC &
    k &= \mbox{Coulomb's Law Constant} = 9 \cdot 10^9 \uNmmpCC
  \end{align*}
  When the two particles have the same ``type'' of charge (positive or negative),
  the force will act to push them apart;
  when they have opposite charges, the force will pull them together.
  
  \section{Circuits}
  \begin{align*}
    V &= IR &
    R_{seq} &= \sum R_{sn} &
    R_{peq} &= \frac{1}{\sum \frac{1}{R_{pn}}} \\
    P &= IV & P &= \frac{V^2}{R} & P &= I^2R \\
    R &= \frac{\rho l}{A}
  \end{align*}
  where
  \begin{align*}
    V &= \mbox{voltage} \uV &
    P &= \mbox{power} \uW \\
    I &= \mbox{amperage} \uA &
    R &= \mbox{resistance} \uOhm \\
    R_{seq} &= \mbox{equivalent resistance in series} \uOhm &
    R_{sn} &= \mbox{resistance of components in series} \uOhm \\
    R_{peq} &= \mbox{equivalent resistance in parallel} \uOhm &
    R_{pn} &= \mbox{resistance of components in parallel} \uOhm \\
    \rho &= \mbox{resistivity} \uOhmm &
    l &= \mbox{length} \um \\
    A &= \mbox{cross-sectional area} \umm
  \end{align*}

  \section{Particles}
  \begin{center}
    $\begin{array}{ c | c c }
      \mbox{particle} & \mbox{mass} \ukg & \mbox{charge} \uC \\
      \hline
      \mbox{proton} & 1.67 \cdot 10^{-27} & +1.60 \cdot 10^{-19} \\
      \mbox{neutron} & 1.67 \cdot 10^{-27} & 0 \\
      \mbox{electron} & 9.11 \cdot 10^{-31} & -1.60 \cdot 10^{-19}
    \end{array}$
  \end{center}

  \section{Unit Symbols}
  \begin{center}
    \begin{tabular}{ c c | c c | c c }
      symbol & name & symbol & name & symbol & name \\
      \hline
      m & meter & kg & kilogram & s & second \\
      A & ampere & V & volt & $\Omega$ & ohm \\
      J & joule & W & watt & C & coulomb \\
      Hz & hertz & N & newton & K & kelvin \\
      rad & radians & deg & degrees &
    \end{tabular}
  \end{center}
  
  \section{SI Prefixes}
  \begin{center}
    \begin{tabular}{ c c c | c c c }
      factor & prefix & symbol & factor & prefix & symbol \\
      \hline
      $10^1$ & deka & da & $10^{-1}$ & deci & d \\
      $10^2$ & hecto & h & $10^{-2}$ & centi & c \\
      $10^3$ & kilo & k & $10^{-3}$ & milli & m \\
      $10^6$ & mega & M & $10^{-6}$ & micro & $\mu$ \\
      $10^9$ & giga & G & $10^{-9}$ & nano & n \\
      $10^{12}$ & tera & T & $10^{-12}$ & pico & p \\
    \end{tabular}
  \end{center}

  \section{Geometry}
  \begin{center}
    $\begin{array}{ c | c c c c }
      \mbox{shape} & A & C & V & S \\
      \hline
      \mbox{rectangle} & bh \\
      \mbox{triangle} & bh \\
      \mbox{circle} & \pi r^2 & 2 \pi r \\
      \mbox{rectangular prism} & & & lwh \\
      \mbox{cylinder} & & & \pi r^2 l & 2 \pi rl + 2 \pi r^2 \\
      \mbox{sphere} & & & \frac{4}{3} \pi r^3 & 4 \pi r^2
    \end{array}$
  \end{center}
  where
  \begin{align*}
    b &= \mbox{base} \um & h &= \mbox{height} \um & r &= \mbox{radius} \um \\
    l &= \mbox{length} \um & w &= \mbox{width} \um & A &= \mbox{area} \umm \\
    C &= \mbox{circumference} \um & V &= \mbox{volume} \ummm &
    S &= \mbox{surface area} \umm
  \end{align*}

\end{document}

